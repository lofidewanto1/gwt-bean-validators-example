package de.knightsoftnet.validationexample.client.resources;

import de.knightsoftnet.navigation.annotation.processor.InfoProperty;
import de.knightsoftnet.navigation.client.version.AbstractVersionInfo;

public class AnnotatedVersionInfo extends AbstractVersionInfo {

  @Override
  @InfoProperty(value = "application.copyright")
  public void setCopyrightText(final String copyrightText) {
    super.setCopyrightText(copyrightText);
  }

  @Override
  @InfoProperty(value = "application.version")
  public void setVersionNumber(final String versionNumber) {
    super.setVersionNumber(versionNumber);
  }

  @Override
  @InfoProperty(value = "build.timestamp")
  public void setVersionDate(final String versionDate) {
    super.setVersionDate(versionDate);
  }

  @Override
  @InfoProperty(value = "application.author")
  public void setAuthor(final String author) {
    super.setAuthor(author);
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.person;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.gwtp.spring.shared.search.SearchFieldDefinition;
import de.knightsoftnet.gwtp.spring.shared.search.TableFieldDefinition;
import de.knightsoftnet.mtwidgets.client.ui.page.admin.AbstractAdminPresenter;
import de.knightsoftnet.mtwidgets.client.ui.request.DeleteRequestPresenter;
import de.knightsoftnet.validationexample.client.resources.ResourcesFile;
import de.knightsoftnet.validationexample.client.services.PersonRestService;
import de.knightsoftnet.validationexample.client.ui.page.person.PersonPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.person.PersonPresenter.MyView;
import de.knightsoftnet.validationexample.shared.models.Person;
import de.knightsoftnet.validationexample.shared.navigation.NameTokens;
import de.knightsoftnet.validators.shared.data.FieldTypeEnum;

import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import org.apache.commons.lang3.StringUtils;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.Objects;

import javax.inject.Inject;

/**
 * Presenter of the person data, implementation.
 *
 * @author Manfred Tremmel
 *
 */
public class PersonPresenter extends AbstractAdminPresenter<Person, MyProxy, MyView> {

  public interface MyView extends AbstractAdminPresenter.MyViewDef<Person, MyProxy, MyView> {
  }

  @ProxyCodeSplit
  @NameToken({NameTokens.PERSON, NameTokens.PERSON_WITH_ID})
  public interface MyProxy extends ProxyPlace<PersonPresenter> {
  }

  /**
   * constructor injecting needed data.
   */
  @Inject
  public PersonPresenter(final EventBus peventBus, final MyView pview, final MyProxy pproxy,
      final ResourceDelegate<PersonRestService> pservice, final Session psession,
      final DeleteRequestPresenter pdeleteRequestPresenter, final ResourcesFile resources,
      final PersonConstants constants) {
    super(peventBus, pview, pproxy, pservice, psession, pdeleteRequestPresenter,
        Arrays.asList(new SearchFieldDefinition("id", constants.id(), FieldTypeEnum.NUMERIC),
            new SearchFieldDefinition("firstName", constants.firstName(), FieldTypeEnum.STRING),
            new SearchFieldDefinition("lastName", constants.lastName(), FieldTypeEnum.STRING),
            new SearchFieldDefinition("email", constants.email(), FieldTypeEnum.STRING),
            new SearchFieldDefinition("birthday", constants.birthday(), FieldTypeEnum.DATE)),
        Arrays.asList(
            new TableFieldDefinition<Person>("id", constants.id(),
                source -> Objects.toString(source.getId(), StringUtils.EMPTY),
                resources.grid().col(), resources.grid().col_1()),
            new TableFieldDefinition<Person>("firstName", constants.firstName(),
                source -> source.getFirstName(), resources.grid().col(), resources.grid().col_3()),
            new TableFieldDefinition<Person>("lastName", constants.lastName(),
                source -> source.getLastName(), resources.grid().col(), resources.grid().col_3()),
            new TableFieldDefinition<Person>("email", constants.email(),
                source -> source.getEmail(), resources.grid().col(), resources.grid().col_3()),
            new TableFieldDefinition<Person>("birthday", constants.birthday(),
                source -> source.getBirthday()
                    .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)),
                resources.grid().col(), resources.grid().col_2())));
  }

  @Override
  protected Person createNewEntry() {
    return new Person();
  }
}

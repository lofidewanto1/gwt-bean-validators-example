/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.client.ui.page.person;

import de.knightsoftnet.mtwidgets.client.ui.page.admin.AbstractAdminView;
import de.knightsoftnet.mtwidgets.client.ui.widget.AdminNavigationWidget;
import de.knightsoftnet.mtwidgets.client.ui.widget.DateBoxLocalDate;
import de.knightsoftnet.mtwidgets.client.ui.widget.EmailTextBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.LongBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.TextBox;
import de.knightsoftnet.validationexample.client.ui.page.person.PersonPresenter.MyProxy;
import de.knightsoftnet.validationexample.client.ui.page.person.PersonPresenter.MyView;
import de.knightsoftnet.validationexample.client.ui.widget.SalutationRadioButton;
import de.knightsoftnet.validationexample.shared.models.Person;
import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import java.time.LocalDate;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * View of the validator test person.
 *
 * @author Manfred Tremmel
 *
 */
public class PersonViewGwtImpl extends AbstractAdminView<Person, MyProxy, MyView>
    implements MyView {

  interface Binder extends UiBinder<Widget, PersonViewGwtImpl> {
  }

  @IsValidationDriver
  interface Driver extends BeanValidationEditorDriver<Person, PersonViewGwtImpl> {
  }

  @UiField
  LongBox id;
  @UiField
  SalutationRadioButton salutation;
  @UiField
  TextBox firstName;
  @UiField
  TextBox lastName;
  @UiField
  EmailTextBox email;
  @UiField
  DateBoxLocalDate birthday;

  /**
   * constructor with injected parameters.
   *
   * @param pdriver editor driver
   * @param puiBinder ui binder
   */
  @Inject
  public PersonViewGwtImpl(final Driver pdriver, final Binder puiBinder,
      final Provider<AdminNavigationWidget<Person>> adminNavigationProvider) {
    super(pdriver, adminNavigationProvider);
    initWidget(puiBinder.createAndBindUi(this));
    driver.initialize(this);
    driver.setSubmitButton(adminNavigation.getSaveEntry());
    driver.addFormSubmitHandler(this);
    birthday.setMax(LocalDate.now());
  }
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package de.knightsoftnet.validationexample.server.controller;

import de.knightsoftnet.gwtp.spring.server.controller.AbstractAdminController;
import de.knightsoftnet.validationexample.server.persistence.dao.PersonPredicatesBuilder;
import de.knightsoftnet.validationexample.server.repository.PersonRepository;
import de.knightsoftnet.validationexample.shared.AppResourcePaths;
import de.knightsoftnet.validationexample.shared.models.Person;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

/**
 * controller implements search functionality.
 *
 * @author Manfred Tremmel
 */
@RestController
@RequestMapping(value = AppResourcePaths.PERSON, produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonController
    extends AbstractAdminController<Person, PersonRepository, PersonPredicatesBuilder> {

  @Inject
  public PersonController(final PersonRepository ppersonalRepository) {
    super(ppersonalRepository, new PersonPredicatesBuilder());
  }
}
